#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "upc.h"
#include "Character.h"

using namespace std;

#define FILAS 30
#define COLUMNAS 130
const uint8_t BLOCK = '\xDB';

void leerLaberintoDesdeArchivo(const string& nombreArchivo, int laberinto[FILAS][COLUMNAS]) {
    ifstream archivo(nombreArchivo);

    if (!archivo) {
        cout << "No se pudo abrir el archivo " << nombreArchivo << endl;
        return;
    }

    string linea;
    for (int i = 0; i < FILAS; ++i) {
        if (!getline(archivo, linea)) break;

        istringstream stream(linea);
        for (int j = 0; j < COLUMNAS; ++j) {
            stream >> laberinto[i][j];
        }
    }
}

void imprimirLaberinto(int laberinto[FILAS][COLUMNAS]) {
    for (int i = 0; i < FILAS; ++i) {
        for (int j = 0; j < COLUMNAS; ++j) {
            switch (laberinto[i][j]) {
            case 0:
                cout << " ";
                break;
            case 1:
                color(DARK_YELLOW);
                cout << BLOCK;
                color(WHITE);
                break;
            case 2:
                color(BRIGHT_YELLOW);
                cout << '\xC8';
                color(WHITE);
                break;
            case 3:
                color(BRIGHT_YELLOW);
                cout << '\xCD';
                color(WHITE);
                break;
            case 4:
                color(BRIGHT_YELLOW);
                cout << '\xCA';
                color(WHITE);
                break;
            case 5:
                color(BRIGHT_CYAN);
                cout << '\xCD';
                color(WHITE);
                break;
            case 6:
                color(BRIGHT_CYAN);
                cout << '\xBC';
                color(WHITE);
                break;
            case 7: // Barrera a desbloquear
                color(BRIGHT_CYAN);
                cout << '\xD7';
                color(WHITE);
                break;
            case 8:
                color(DARK_YELLOW);
                cout << '\xBA';
                color(WHITE);
                break;
            case 9:
                color(DARK_YELLOW);
                cout << '\xB0';
                color(WHITE);
                break;
            default:
                cout << " ";
                break;
            }
        }

        if (i < FILAS - 1) {
            cout << endl;
        }
    }
}

void actualizarPosicionPersonaje(const Character& character) {
    const string* sprite = (character.isFacingRight)
        ? (character.isFrame1 ? character.spriteRight1 : character.spriteRight2)
        : (character.isFrame1 ? character.spriteLeft1 : character.spriteLeft2);

    for (int i = 0; i < SPRITE_HEIGHT; ++i) {
        gotoxy(character.x, character.y + i);
        cout << sprite[i];
    }
}

bool checkCollisionWithMap(int newX, int newY, const int laberinto[FILAS][COLUMNAS]) {
    for (int i = 0; i < SPRITE_HEIGHT; ++i) {
        for (int j = 0; j < SPRITE_WIDTH; ++j) {
            int x = newX + j;
            int y = newY + i;
            if (x < 0 || x >= COLUMNAS || y < 0 || y >= FILAS ||
                (laberinto[y][x] != 0 && !(keyCollected && laberinto[y][x] == 7))) {
                return true;
            }
        }
    }
    return false;
}