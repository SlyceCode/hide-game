#pragma once
#include <iostream>
#include <vector>
#include "upc.h"
#include "Character.h"

using namespace std;

#define BARREL_WIDTH 7
#define BARREL_HEIGHT 6
#define TOTAL_CAPSULES_THRESHOLD1 2

struct Barrel {
    int x, y;
    bool hasCapsule;
    int capsuleCount;
    int maxCapsules;
    int predefinedValue;
    bool isLocked;
    string sprite[BARREL_HEIGHT] = {
        " _____",
        "/     \\",
        "|     |",
        "|     |",
        "|     |",
        "\\_____/"
    };
};

Barrel createBarrel(int startX, int startY, int maxCapsules, int predefinedValue = 0, bool isLocked = false) {
    Barrel barrel;
    barrel.x = startX;
    barrel.y = startY;
    barrel.hasCapsule = false;
    barrel.capsuleCount = predefinedValue;
    barrel.maxCapsules = maxCapsules;
    barrel.predefinedValue = predefinedValue;
    barrel.isLocked = isLocked;
    return barrel;
}

void drawBarrel(const Barrel& barrel) {
    if (barrel.predefinedValue == TOTAL_CAPSULES_THRESHOLD1) {
        color(BRIGHT_CYAN);
    }
    else {
        color(DARK_YELLOW);
    }

    for (int i = 0; i < BARREL_HEIGHT; ++i) {
        gotoxy(barrel.x, barrel.y + i);
        cout << barrel.sprite[i];
    }

    color(WHITE);

    gotoxy(barrel.x + 3, barrel.y + 3);
    if (barrel.predefinedValue == TOTAL_CAPSULES_THRESHOLD1) {
        color(BRIGHT_CYAN);
        cout << barrel.predefinedValue;
    }
    else if (barrel.predefinedValue != 0) {
        color(DARK_YELLOW);
        cout << barrel.predefinedValue;
    }
    else {
        color(WHITE);
        cout << barrel.capsuleCount;
    }
}

void clearBarrel(const Barrel& barrel) {
    for (int i = 0; i < BARREL_HEIGHT; ++i) {
        gotoxy(barrel.x, barrel.y + i);
        cout << "       ";
    }

    gotoxy(barrel.x + 2, barrel.y - 1);
    cout << "     ";
}
