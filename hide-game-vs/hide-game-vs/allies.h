#pragma once
#include <iostream>
#include "upc.h"

using namespace std;

#define ALLY_WIDTH 12  
#define ALLY_HEIGHT 6  

struct Ally {
    int x, y;
    int health;  // Salud del aliado
    string sprite[ALLY_HEIGHT] = {
    "             ____  ",
    "            / . .\\ ",
    "            \\  ---<",
    "             \\  /  ",
    "   __________/ /   ",
    "-=:___________/    ",
    };
};

Ally createAlly(int startX, int startY) {
    Ally ally;
    ally.x = startX;
    ally.y = startY;
    ally.health = 50;  
    return ally;
}

void drawAlly(const Ally& ally) {
    color(BRIGHT_CYAN);
    for (int i = 0; i < ALLY_HEIGHT; ++i) {
        gotoxy(ally.x, ally.y + i);
        cout << ally.sprite[i];
    }
    color(WHITE);
}

void clearAlly(const Ally& ally) {
    for (int i = 0; i < ALLY_HEIGHT; ++i) {
        gotoxy(ally.x, ally.y + i);
        cout << string(ALLY_WIDTH, ' ');
    }
}
