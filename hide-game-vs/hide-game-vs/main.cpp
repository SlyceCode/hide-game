﻿#include <iostream>
#include <windows.h>
#include <vector>
#include "upc.h"
#include "Displays.h"
#include "Controlador.h"
#include "capsule.h"
#include "keys.h"

using namespace std;
using uint = unsigned int;

void updateCapsuleCounter(int count);
void handleMenuOption(int option);

int capsulesCollected = 0;
vector<Capsule> capsules;
vector<Capsule> collectedCapsules;
int capsuleIndex = 0;
vector<Barrel> barrels;
vector<Key> keys;


void handleMenuOption(int option) {
    if (option == '1') {
        level1();
    }
    else if (option == '2') {
        rules();
    }
    else if (option == '3') {
        credits();
    }
}

int main() {
    hideCursor();

    int option;
    do {
        mainMenu();
        option = _getch();
        if (option < '0' || option > '3') {
            error();
        }
        else {
            handleMenuOption(option);
        }
    } while (option != '0');

    showCursor();
    return EXIT_SUCCESS;
}