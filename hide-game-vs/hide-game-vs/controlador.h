#pragma once
#include "upc.h"
#include "Displays.h"
#include "Character.h"
#include "capsule.h"
#include "Barrel.h"
#include "keys.h"
#include "Monster.h"
#include "maps.h"

#include <conio.h>

using namespace std;
using uint = unsigned int;

#define BARREL_WIDTH 7
#define BARREL_HEIGHT 6
#define TOTAL_CAPSULES_THRESHOLD1 2
#define TOTAL_CAPSULES_THRESHOLD2 4
#define TOTAL_CAPSULES_THRESHOLD3 -2


extern int capsulesCollected;
extern vector<Capsule> capsules;
extern vector<Capsule> collectedCapsules;
extern int capsuleIndex;
extern vector<Barrel> barrels;
extern vector<Key> keys;

void updateCapsuleCounter(int count);
void updateCapsuleCounter2(int count);

bool isPositionFree(int x, int y) {
    for (const auto& barrel : barrels) {
        if (barrel.isLocked) {
            int barrelLeft = barrel.x;
            int barrelRight = barrel.x + BARREL_WIDTH;
            int barrelTop = barrel.y;
            int barrelBottom = barrel.y + BARREL_HEIGHT;

            if (x < barrelRight && x + SPRITE_WIDTH > barrelLeft && y < barrelBottom && y + SPRITE_HEIGHT > barrelTop) {
                return false;
            }
        }
    }
    return true;
}

void win() {
    clear();

    ConsoleInfo ci;
    getConsoleInfo(&ci, 0, 0, 0, 0);
    drawBorder(ci);

    int centerX = (ci.right - ci.left + 1) / 2;
    int centerY = (ci.bottom - ci.top + 1) / 2;

    color(BRIGHT_YELLOW);

    printxy(centerX - 10, centerY - 9, " \xDB\xDB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xBB   \xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
    printxy(centerX - 10, centerY - 8, "\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC \xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xDB\xDB\xC9\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC");
    printxy(centerX - 10, centerY - 7, "\xDB\xDB\xBA  \xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA\xDB\xDB\xC9\xDB\xDB\xBB \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB   \xDB\xDB\xBA   \xDB\xDB\xDB\xDB\xDB\xBB  ");
    printxy(centerX - 10, centerY - 6, "\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBA\xDB\xDB\xBA\xC8\xDB\xDB\xBB\xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBA\xC8\xCD\xCD\xCD\xCD\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xC9\xCD\xCD\xBC  ");
    printxy(centerX - 10, centerY - 5, "\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xBA  \xDB\xDB\xBA\xDB\xDB\xBA \xC8\xDB\xDB\xDB\xDB\xBA\xDB\xDB\xBA  \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
    printxy(centerX - 10, centerY - 4, " \xC8\xCD\xCD\xCD\xCD\xCD\xBC \xC8\xCD\xBC  \xC8\xCD\xBC\xC8\xCD\xBC  \xC8\xCD\xCD\xCD\xBC\xC8\xCD\xBC  \xC8\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC   \xC8\xCD\xBC   \xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC");

    printxy(25, 13, "                       .-++++=:                                                 ");
    printxy(25, 14, "                       .*%%@@@=    LEIKO DESPERTO, Y SU HERMANA ESTABA          ");
    printxy(25, 15, "                         .=@@=     DURMIENDO EN EL SILLON...                    ");
    printxy(25, 16, "                        .*@@-                                                   ");
    printxy(25, 17, "                       .*@@-::.                                                 ");
    printxy(25, 18, "             .-=====.             =+.      .*=.      .%=.     -:               ");
    printxy(25, 19, "             .%@@@@@:             .*@       -%:      +#.    .*# ");
    printxy(25, 20, "                -@@-.      ..       -@-      +%.    .%=   ..**.           .:.        ");
    printxy(25, 21, "              .+@@=.       =#-.      :%+..:-=*@@#*==*%.%#::#+.         .=@=.      ");
    printxy(25, 22, "             .*@@-...      .:*#:     .:%%#+-:..            $#%+#       .**. ");
    printxy(25, 23, "             -%@@@@@-        .:%#:  :#%=..                   ..-#@-$#%%:%=.   ");
    printxy(25, 24, "    -*****+           .*#.      :@+*@.                           $$#%:        .-:  ");
    printxy(25, 25, "     -**#@@*            .-%=.    .:#+.                            .=#      ..-%#-.  ");
    printxy(25, 26, "      :%@*.              .*%:   .*+.                              .=@-      :*#+.     ");
    printxy(25, 27, "     :%@+.                 -#=. **.                                .+*   .+%*:.      ");
    printxy(25, 28, "    -@@#=--                ..*%:@:                                  .%--##-.   ");

    esperarTecla('9');
}

void clearLevelElements() {
    for (const auto& barrel : barrels) {
        clearBarrel(barrel);
    }
    barrels.clear();

    for (const auto& capsule : capsules) {
        clearCapsule(capsule);
    }
    capsules.clear();

    collectedCapsules.clear();

    keys.clear();
}

void level3() {
    capsulesCollected = 0;
    keyCollected = false;
    clear();

    int laberinto[FILAS][COLUMNAS];
    leerLaberintoDesdeArchivo("laberinto3.txt", laberinto);
    imprimirLaberinto(laberinto);

    updateCapsuleCounter2(0);

    Character character = createCharacter(10, 13);
    drawCharacter(character);

    barrels.push_back(createBarrel(63, 22, 10));
    barrels.push_back(createBarrel(78, 22, 10));
    barrels.push_back(createBarrel(94, 22, 0, -3, true));
    barrels.push_back(createBarrel(109, 22, 0, TOTAL_CAPSULES_THRESHOLD3, true));

    capsules.push_back(createCapsule(8, 24));
    capsules.push_back(createCapsule(22, 13));
    capsules.push_back(createCapsule(47, 13));
    capsules.push_back(createCapsule(67, 7));

    capsules.push_back(createCapsule(49, 3, true)); 
    capsules.push_back(createCapsule(80, 4, true)); 
    capsules.push_back(createCapsule(85, 16, true));

    keys.push_back(createKey(10, 13));

    for (const auto& barrel : barrels) {
        drawBarrel(barrel);
    }

    for (const auto& capsule : capsules) {
        drawCapsule(capsule);
    }

    Monster monster = createMonster(96, 7);
    drawMonster(monster, 3); 

    capsuleIndex = 0;

    char key;
    int prevX = character.x;
    int prevY = character.y;

    do {
        key = _getch();
        prevX = character.x;
        prevY = character.y;

        if (key == 'w' || key == 'a' || key == 's' || key == 'd') {
            clearCharacter(character);
            updateCapsuleCounter2(capsulesCollected);

            switch (key) {
            case 'w': character.y--; break;
            case 'a': character.x--; character.isFacingRight = false; break;
            case 's': character.y++; break;
            case 'd': character.x++; character.isFacingRight = true; break;
            }

            for (auto& key : keys) {
                if (checkKeyCollision(character, key) && key.visible) {
                    clearKey(key);
                    key.visible = false;
                    keyCollected = true;
                    break;
                }
            }

            bool collision = false;
            for (const auto& barrel : barrels) {
                if (checkBarrelCollision(character, barrel)) {
                    collision = true;
                    break;
                }
            }

            if (checkCollisionWithMap(character.x, character.y, laberinto) || collision) {
                if (!keyCollected || (keyCollected && laberinto[character.y][character.x] != 7)) {
                    character.x = prevX;
                    character.y = prevY;
                }
            }

            actualizarPosicionPersonaje(character);

            for (auto it = capsules.begin(); it != capsules.end();) {
                if (checkCollision(character, *it)) {
                    clearCapsule(*it);
                    capsulesCollected++;
                    updateCapsuleCounter2(capsulesCollected);
                    collectedCapsules.push_back(*it);
                    it = capsules.erase(it);
                }
                else {
                    ++it;
                }
            }
        }
        else if (key == 'e') {
            if (!collectedCapsules.empty()) {
                Capsule capsule = collectedCapsules.back();
                collectedCapsules.pop_back();

                if (character.isFacingRight) {
                    capsule.x = character.x + SPRITE_WIDTH;
                }
                else {
                    capsule.x = character.x - SPRITE_WIDTH;
                }
                capsule.y = character.y;
                capsule.collected = false;

                if (!checkCollisionWithMap(capsule.x, capsule.y, laberinto) && isPositionFree(capsule.x, capsule.y)) {
                    bool placedInBarrel = false;
                    for (auto& barrel : barrels) {
                        if (!barrel.isLocked && checkCollisionWithBarrel(capsule, barrel) && barrel.capsuleCount < barrel.maxCapsules) {
                            barrel.hasCapsule = true;
                            barrel.capsuleCount += capsule.value;  
                            placedInBarrel = true;
                            break;
                        }
                    }

                    if (!placedInBarrel) {
                        capsules.push_back(capsule);
                        drawCapsule(capsule);
                    }
                    capsulesCollected--;
                    updateCapsuleCounter2(capsulesCollected);
                }
                else {
                    collectedCapsules.push_back(capsule);
                }
            }
        }

        else if (key == 'r') {
            for (auto& barrel : barrels) {
                if (!barrel.isLocked && barrel.capsuleCount != 0 &&
                    ((character.isFacingRight && character.x + SPRITE_WIDTH == barrel.x) ||
                        (!character.isFacingRight && character.x == barrel.x + BARREL_WIDTH))) {

                    int capsuleValue = (barrel.capsuleCount > 0) ? 1 : -1; 
                    Capsule capsule = createCapsule(barrel.x + 1, barrel.y - 3, capsuleValue == -1);
                    barrel.capsuleCount -= capsuleValue; 

                    capsules.push_back(capsule);
                    updateCapsuleCounter2(capsulesCollected);
                    break;
                }
            }
        }

        if (checkMonsterCollision(character, monster, 3)) { 
            combatSystem(character, monster, false, true); 
            if (monster.health <= 0) {
                clearLevelElements();
                clear();
                win();
                break;
            }
            else {
                clear();
                drawCharacter(character);
                for (const auto& capsule : capsules) {
                    drawCapsule(capsule);
                }
                for (const auto& barrel : barrels) {
                    drawBarrel(barrel);
                }
                drawMonster(monster, 3); 
            }
        }

        int totalCapsulesInBarrels = 0 - TOTAL_CAPSULES_THRESHOLD3;
        for (const auto& barrel : barrels) {
            if (barrel.isLocked) {
                totalCapsulesInBarrels += barrel.predefinedValue;
            }
            else {
                totalCapsulesInBarrels += barrel.capsuleCount;
            }
        }

        for (auto& key : keys) {
            if (keyCollected) {
                clearKey(key);
                continue;
            }
            if (totalCapsulesInBarrels == TOTAL_CAPSULES_THRESHOLD3) { 
                if (!key.visible) {
                    key.visible = true;
                    drawKey(key);
                }
            }
            else {
                if (key.visible) {
                    clearKey(key);
                    key.visible = false;
                    break;
                }
            }
        }

        for (const auto& capsule : capsules) {
            drawCapsule(capsule);
        }

        for (const auto& barrel : barrels) {
            drawBarrel(barrel);
        }

    } while (key != 'q');
}


void level2() {
    keyCollected = false;
    clear();

    int laberinto[FILAS][COLUMNAS];
    leerLaberintoDesdeArchivo("laberinto2.txt", laberinto);
    imprimirLaberinto(laberinto);

    updateCapsuleCounter2(0);

    Character character = createCharacter(60, 13);
    drawCharacter(character);

    barrels.push_back(createBarrel(63, 22, 10));
    barrels.push_back(createBarrel(78, 22, 10));
    barrels.push_back(createBarrel(94, 22, 0, -3, true));
    barrels.push_back(createBarrel(109, 22, 0, TOTAL_CAPSULES_THRESHOLD2, true));

    capsules.push_back(createCapsule(10, 4));
    capsules.push_back(createCapsule(12, 24));
    capsules.push_back(createCapsule(40, 22));
    capsules.push_back(createCapsule(67, 7));
    capsules.push_back(createCapsule(66, 2));
    capsules.push_back(createCapsule(120, 8));
    capsules.push_back(createCapsule(120, 17));

    keys.push_back(createKey(75, 13));

    for (const auto& barrel : barrels) {
        drawBarrel(barrel);
    }

    for (const auto& capsule : capsules) {
        drawCapsule(capsule);
    }

    Monster monster = createMonster(5, 12);
    drawMonster(monster, 2); 

    capsuleIndex = 0;

    char key;
    int prevX = character.x;
    int prevY = character.y;

    do {
        key = _getch();
        prevX = character.x;
        prevY = character.y;

        if (key == 'w' || key == 'a' || key == 's' || key == 'd') {
            clearCharacter(character);
            updateCapsuleCounter2(capsulesCollected);

            switch (key) {
            case 'w': character.y--; break;
            case 'a': character.x--; character.isFacingRight = false; break;
            case 's': character.y++; break;
            case 'd': character.x++; character.isFacingRight = true; break;
            }

            for (auto& key : keys) {
                if (checkKeyCollision(character, key) && key.visible) {
                    clearKey(key);
                    key.visible = false;
                    keyCollected = true;
                    break;
                }
            }

            bool collision = false;
            for (const auto& barrel : barrels) {
                if (checkBarrelCollision(character, barrel)) {
                    collision = true;
                    break;
                }
            }

            if (checkCollisionWithMap(character.x, character.y, laberinto) || collision) {
                if (!keyCollected || (keyCollected && laberinto[character.y][character.x] != 7)) {
                    character.x = prevX;
                    character.y = prevY;
                }
            }

            actualizarPosicionPersonaje(character);

            for (auto it = capsules.begin(); it != capsules.end();) {
                if (checkCollision(character, *it)) {
                    clearCapsule(*it);
                    capsulesCollected++;
                    updateCapsuleCounter2(capsulesCollected);
                    collectedCapsules.push_back(*it);
                    it = capsules.erase(it);
                }
                else {
                    ++it;
                }
            }
        }
        else if (key == 'e') {
            if (!collectedCapsules.empty()) {
                Capsule capsule = collectedCapsules.back();
                collectedCapsules.pop_back();

                if (character.isFacingRight) {
                    capsule.x = character.x + SPRITE_WIDTH;
                }
                else {
                    capsule.x = character.x - SPRITE_WIDTH;
                }
                capsule.y = character.y;
                capsule.collected = false;

                if (!checkCollisionWithMap(capsule.x, capsule.y, laberinto) && isPositionFree(capsule.x, capsule.y)) {
                    bool placedInBarrel = false;
                    for (auto& barrel : barrels) {
                        if (!barrel.isLocked && checkCollisionWithBarrel(capsule, barrel) && barrel.capsuleCount < barrel.maxCapsules) {
                            barrel.hasCapsule = true;
                            barrel.capsuleCount++;
                            placedInBarrel = true;
                            break;
                        }
                    }

                    if (!placedInBarrel) {
                        capsules.push_back(capsule);
                        drawCapsule(capsule);
                    }
                    capsulesCollected--;
                    updateCapsuleCounter2(capsulesCollected);
                }
                else {
                    collectedCapsules.push_back(capsule);
                }
            }
        }

        else if (key == 'r') {
            for (auto& barrel : barrels) {
                if (!barrel.isLocked && barrel.capsuleCount > 0 &&
                    ((character.isFacingRight && character.x + SPRITE_WIDTH == barrel.x) ||
                        (!character.isFacingRight && character.x == barrel.x + BARREL_WIDTH))) {
                    barrel.capsuleCount--;
                    Capsule capsule = createCapsule(barrel.x + 1, barrel.y - 3);
                    capsules.push_back(capsule);
                    updateCapsuleCounter2(capsulesCollected);
                    break;
                }
            }
        }

        if (checkMonsterCollision(character, monster, 2)) { 
            combatSystem(character, monster, 2);
            if (monster.health <= 0) {
                clearLevelElements();
                clear();
                nextLevel();
                clear();

                level3();
                break;
            }
            else {
                clear();
                drawCharacter(character);
                for (const auto& capsule : capsules) {
                    drawCapsule(capsule);
                }
                for (const auto& barrel : barrels) {
                    drawBarrel(barrel);
                }
                drawMonster(monster, 2); 
            }
        }

        int totalCapsulesInBarrels = 0 - TOTAL_CAPSULES_THRESHOLD2;
        for (const auto& barrel : barrels) {
            if (barrel.isLocked) {
                totalCapsulesInBarrels += barrel.predefinedValue;
            }
            else {
                totalCapsulesInBarrels += barrel.capsuleCount;

            }
        }

        for (auto& key : keys) {
            if (keyCollected) {
                clearKey(key);
                continue;
            }
            if (totalCapsulesInBarrels == TOTAL_CAPSULES_THRESHOLD2) {
                if (!key.visible) {
                    key.visible = true;
                    drawKey(key);
                }
            }
            else {
                if (key.visible) {
                    clearKey(key);
                    key.visible = false;
                    break;
                }
            }
        }

        for (const auto& capsule : capsules) {
            drawCapsule(capsule);
        }

        for (const auto& barrel : barrels) {
            drawBarrel(barrel);
        }

    } while (key != 'q');
}


void level1() {
    clearLevelElements();
    clear();

    int laberinto[FILAS][COLUMNAS];
    leerLaberintoDesdeArchivo("laberinto.txt", laberinto);
    imprimirLaberinto(laberinto);

    updateCapsuleCounter(0);

    Character character = createCharacter(6, 25);
    drawCharacter(character);

    barrels.push_back(createBarrel(49, 22, 10));
    barrels.push_back(createBarrel(64, 22, 10));
    barrels.push_back(createBarrel(79, 22, 10));
    barrels.push_back(createBarrel(94, 22, 0, -2, true));
    barrels.push_back(createBarrel(109, 22, 0, TOTAL_CAPSULES_THRESHOLD1, true));

    capsules.push_back(createCapsule(9, 5));
    capsules.push_back(createCapsule(31, 10));
    capsules.push_back(createCapsule(28, 24));
    capsules.push_back(createCapsule(67, 10));
    capsules.push_back(createCapsule(90, 5));

    keys.push_back(createKey(75, 5));

    for (const auto& capsule : capsules) {
        drawCapsule(capsule);
    }

    for (const auto& barrel : barrels) {
        drawBarrel(barrel);
    }

    Monster monster = createMonster(102, 2);
    drawMonster(monster);

    capsuleIndex = 0;

    char key;
    int prevX = character.x;
    int prevY = character.y;

    do {
        key = _getch();
        prevX = character.x;
        prevY = character.y;

        if (key == 'w' || key == 'a' || key == 's' || key == 'd') {
            clearCharacter(character);
            updateCapsuleCounter(capsulesCollected);

            switch (key) {
            case 'w': character.y--; break;
            case 'a': character.x--; character.isFacingRight = false; break;
            case 's': character.y++; break;
            case 'd': character.x++; character.isFacingRight = true; break;
            }

            for (auto& key : keys) {
                if (checkKeyCollision(character, key) && key.visible) {
                    clearKey(key);
                    key.visible = false;
                    keyCollected = true;
                    break;
                }
            }

            bool collision = false;
            for (const auto& barrel : barrels) {
                if (checkBarrelCollision(character, barrel)) {
                    collision = true;
                    break;
                }
            }

            if (checkCollisionWithMap(character.x, character.y, laberinto) || collision) {
                if (!keyCollected || (keyCollected && laberinto[character.y][character.x] != 7)) {
                    character.x = prevX;
                    character.y = prevY;
                }
            }

            actualizarPosicionPersonaje(character);

            for (auto it = capsules.begin(); it != capsules.end();) {
                if (checkCollision(character, *it)) {
                    clearCapsule(*it);
                    capsulesCollected++;
                    updateCapsuleCounter(capsulesCollected);
                    collectedCapsules.push_back(*it);
                    it = capsules.erase(it);
                }
                else {
                    ++it;
                }
            }
        }
        else if (key == 'e') {
            if (!collectedCapsules.empty()) {
                Capsule capsule = collectedCapsules.back();
                collectedCapsules.pop_back();

                if (character.isFacingRight) {
                    capsule.x = character.x + SPRITE_WIDTH;
                }
                else {
                    capsule.x = character.x - SPRITE_WIDTH;
                }
                capsule.y = character.y;
                capsule.collected = false;

                if (!checkCollisionWithMap(capsule.x, capsule.y, laberinto) && isPositionFree(capsule.x, capsule.y)) {
                    bool placedInBarrel = false;
                    for (auto& barrel : barrels) {
                        if (!barrel.isLocked && checkCollisionWithBarrel(capsule, barrel) && barrel.capsuleCount < barrel.maxCapsules) {
                            barrel.hasCapsule = true;
                            barrel.capsuleCount++;
                            placedInBarrel = true;
                            break;
                        }
                    }

                    if (!placedInBarrel) {
                        capsules.push_back(capsule);
                        drawCapsule(capsule);
                    }
                    capsulesCollected--;
                    updateCapsuleCounter(capsulesCollected);
                }
                else {
                    collectedCapsules.push_back(capsule);
                }
            }
        }

        else if (key == 'r') {
            for (auto& barrel : barrels) {
                if (!barrel.isLocked && barrel.capsuleCount > 0 &&
                    ((character.isFacingRight && character.x + SPRITE_WIDTH == barrel.x) ||
                        (!character.isFacingRight && character.x == barrel.x + BARREL_WIDTH))) {
                    barrel.capsuleCount--;
                    Capsule capsule = createCapsule(barrel.x + 1, barrel.y - 3);
                    capsules.push_back(capsule);
                    updateCapsuleCounter(capsulesCollected);
                    break;
                }
            }
        }

        if (checkMonsterCollision(character, monster, true)) {
            combatSystem(character, monster, false);
            if (monster.health <= 0) {
                clearLevelElements();
                clear();
                nextLevel();
                clear();

                level2();
                break;
            }
            else {
                clear();
                drawCharacter(character);
                for (const auto& capsule : capsules) {
                    drawCapsule(capsule);
                }
                for (const auto& barrel : barrels) {
                    drawBarrel(barrel);
                }
                drawMonster(monster);
            }
        }

        for (auto& barrel : barrels) {
            clearBarrel(barrel);
            drawBarrel(barrel);
        }

        int totalCapsulesInBarrels = 0 - TOTAL_CAPSULES_THRESHOLD1;
        for (const auto& barrel : barrels) {
            if (barrel.isLocked) {
                totalCapsulesInBarrels += barrel.predefinedValue;
            }
            else {
                totalCapsulesInBarrels += barrel.capsuleCount;
            }
        }

        for (auto& key : keys) {
            if (keyCollected) {
                clearKey(key);
                continue;
            }
            if (totalCapsulesInBarrels == TOTAL_CAPSULES_THRESHOLD1) {
                if (!key.visible) {
                    key.visible = true;
                    drawKey(key);
                }
            }
            else {
                if (key.visible) {
                    clearKey(key);
                    key.visible = false;
                    break;
                }
            }
        }

        for (const auto& capsule : capsules) {
            drawCapsule(capsule);
        }


    } while (key != 'q');
}