#pragma once
#include <iostream>
#include "upc.h"

using namespace std;

struct Key {
    int x, y;
    string sprite[1] = {
        "]}-->"
    };
    bool visible;
};

Key createKey(int startX, int startY) {
    Key key;
    key.x = startX;
    key.y = startY;
    key.visible = false;
    return key;
}

void clearKey(const Key& key) {
    gotoxy(key.x, key.y);
    cout << "     ";  
}

void drawKey(const Key& key) {
    if (key.visible) {
        gotoxy(key.x, key.y);
        cout << key.sprite[0];
    }
}

bool checkKeyCollision(const Character& character, const Key& key) {
    int charLeft = character.x;
    int charRight = character.x + SPRITE_WIDTH;
    int charTop = character.y;
    int charBottom = character.y + SPRITE_HEIGHT;

    int keyLeft = key.x;
    int keyRight = key.x + key.sprite[0].length();
    int keyTop = key.y;
    int keyBottom = key.y + 1;

    return key.visible &&
        charRight > keyLeft &&
        charLeft < keyRight &&
        charBottom > keyTop &&
        charTop < keyBottom;
}

bool keyCollected = false; 