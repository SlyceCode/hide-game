#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "upc.h"
#include "Barrel.h"

using namespace std;
using uint = unsigned int;

#define COL 130
#define FIL 30
#define SPRITE_HEIGHT 3
#define SPRITE_WIDTH 5

#define BARREL_WIDTH 7 
#define BARREL_HEIGHT 6

struct Character {
    int x, y;
    int health;
    string spriteRight1[SPRITE_HEIGHT];
    string spriteRight2[SPRITE_HEIGHT];
    string spriteLeft1[SPRITE_HEIGHT];
    string spriteLeft2[SPRITE_HEIGHT];
    bool isFrame1;
    bool isFacingRight;
};

Character createCharacter(int startX, int startY) {
    Character character;
    character.x = startX;
    character.y = startY;
    character.health = 100;

    string sprites[4][SPRITE_HEIGHT] = {
        {"  \xE5 /", "0(|-", " / \\"},  // spriteRight1
        {"  \xE5 |", "0(|v'", " / |"},  // spriteRight2 
        {"\\ \xE5  ", " -|)0", " / \\"}, // spriteLeft1
        {"\\ \xE5  ", "'v(|0", " | \\"}  // spriteLeft2
    };

    for (int i = 0; i < SPRITE_HEIGHT; ++i) {
        character.spriteRight1[i] = sprites[0][i];
        character.spriteRight2[i] = sprites[1][i];
        character.spriteLeft1[i] = sprites[2][i];
        character.spriteLeft2[i] = sprites[3][i];
    }

    character.isFrame1 = true;
    character.isFacingRight = true;

    return character;
}

void drawCharacter(const Character& character) {
    color(WHITE);

    const string* sprite = (character.isFacingRight)
        ? (character.isFrame1 ? character.spriteRight1 : character.spriteRight2)
        : (character.isFrame1 ? character.spriteLeft1 : character.spriteLeft2);

    for (int i = 0; i < SPRITE_HEIGHT; ++i) {
        gotoxy(character.x, character.y + i);
        cout << sprite[i];
    }
}

void clearCharacter(const Character& character) {
    for (int i = 0; i < SPRITE_HEIGHT; ++i) {
        gotoxy(character.x, character.y + i);
        cout << string(SPRITE_WIDTH, ' ');
    }
}

bool checkBarrelCollision(const Character& character, const Barrel& barrel) {
    int charLeft = character.x;
    int charRight = character.x + SPRITE_WIDTH;
    int charTop = character.y;
    int charBottom = character.y + SPRITE_HEIGHT;

    int barrelLeft = barrel.x;
    int barrelRight = barrel.x + BARREL_WIDTH;
    int barrelTop = barrel.y;
    int barrelBottom = barrel.y + BARREL_HEIGHT;

    return charRight > barrelLeft &&
        charLeft < barrelRight &&
        charBottom > barrelTop &&
        charTop < barrelBottom;
}

void moveCharacter(Character& character, char direction, const vector<Barrel>& barrels) {
    int prevX = character.x;
    int prevY = character.y;

    clearCharacter(character);

    switch (direction) {
    case 'w':
        if (character.y > 0) {
            character.y--;
        }
        break;
    case 's':
        if (character.y < FIL - SPRITE_HEIGHT) {
            character.y++;
        }
        break;
    case 'a':
        if (character.x > 0) {
            character.x--;
            character.isFacingRight = false;
        }
        break;
    case 'd':
        if (character.x < COL - SPRITE_WIDTH) {
            character.x++;
            character.isFacingRight = true;
        }
        break;
    }

    bool collision = false;
    for (const auto& barrel : barrels) {
        if (checkBarrelCollision(character, barrel)) {
            collision = true;
            break;
        }
    }

    if (collision) {
        character.x = prevX;
        character.y = prevY;
    }
    else {
        character.isFrame1 = !character.isFrame1;
    }

    drawCharacter(character);
}