#pragma once
#include <iostream>
#include <vector>
#include "upc.h"
#include "barrel.h"
#include "Character.h"

using namespace std;
#define SPRITE_HEIGHT 3
#define SPRITE_WIDTH 5

struct Capsule {
    int x, y;
    bool collected;
    int value;  
    string sprite[3] = {
        " ___ ",
        "{ + }",
        "'---'"
    };
    string redSprite[3] = {  
        " ___ ",
        "{ - }",
        "'---'"
    };
};

Capsule createCapsule(int startX, int startY, bool isRed = false) {
    Capsule capsule;
    capsule.x = startX;
    capsule.y = startY;
    capsule.collected = false;
    capsule.value = isRed ? -1 : 1;  
    return capsule;
}

void drawCapsule(const Capsule& capsule) {
    if (!capsule.collected) {
        if (capsule.value == -1) {  
            color(BRIGHT_RED);
            for (int i = 0; i < 3; ++i) {
                gotoxy(capsule.x, capsule.y + i);
                cout << capsule.redSprite[i];
            }
        }
        else {  
            color(BRIGHT_CYAN);
            for (int i = 0; i < 3; ++i) {
                gotoxy(capsule.x, capsule.y + i);
                cout << capsule.sprite[i];
            }
        }
    }
    color(WHITE);
}

void clearCapsule(const Capsule& capsule) {
    for (int i = 0; i < 3; ++i) {
        gotoxy(capsule.x, capsule.y + i);
        cout << "     ";
    }
}

bool checkCollision(const Character& character, const Capsule& capsule) {
    return !capsule.collected &&
        character.x < capsule.x + SPRITE_WIDTH &&
        character.x + SPRITE_WIDTH > capsule.x &&
        character.y < capsule.y + SPRITE_HEIGHT &&
        character.y + SPRITE_HEIGHT > capsule.y;
}

bool checkCollisionWithBarrel(const Capsule& capsule, const Barrel& barrel) {
    return capsule.x < barrel.x + BARREL_WIDTH &&
        capsule.x + SPRITE_WIDTH > barrel.x &&
        capsule.y < barrel.y + BARREL_HEIGHT &&
        capsule.y + SPRITE_HEIGHT > barrel.y;
}

void handleCapsuleInBarrel(vector<Capsule>& capsules, const Character& character, Barrel& barrel) {
    for (auto it = capsules.begin(); it != capsules.end();) {
        Capsule& capsule = *it;
        if (checkCollision(character, capsule)) {
            clearCapsule(capsule);
            barrel.hasCapsule = true;
            barrel.capsuleCount += capsule.value;  
            it = capsules.erase(it);
        }
        else {
            ++it;
        }
    }
}
