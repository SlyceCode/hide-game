﻿#pragma once

#include "upc.h"

using uint = unsigned int;
using namespace std;

void printxy(uint x, int y, string txt) {
	gotoxy(x, y);
	cout << txt;
}

int esperarTecla(char teclaEsperada) {
	int regresar;
	do {
		regresar = _getch();
	} while (regresar != teclaEsperada);
	return regresar;
}

void drawBorder(ConsoleInfo& ci) {
	color(WHITE);
	for (int x = ci.left; x <= ci.right; ++x) {
		gotoxy(x, ci.top);
		cout << "\xDB";
		gotoxy(x, ci.bottom);
		cout << "\xDB";
	}
	for (int y = ci.top; y <= ci.bottom; ++y) {
		gotoxy(ci.left, y);
		cout << "\xDB";
		gotoxy(ci.right, y - 1);
		cout << "\xDB" << endl;
	}
}

void mainMenu() {
	clear();

	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);
	drawBorder(ci);

	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;

	printxy(centerX - 15, centerY - 9, "\xDB\xDB\xBB  \xDB\xDB\xBB \xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 15, centerY - 8, "\xDB\xDB\xBA  \xDB\xDB\xBA \xDB\xDB\xBA \xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB \xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC");
	printxy(centerX - 15, centerY - 7, "\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA \xDB\xDB\xBA \xDB\xDB\xBA  \xDB\xDB\xBA \xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 15, centerY - 6, "\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBA \xDB\xDB\xBA \xDB\xDB\xBA  \xDB\xDB\xBA \xDB\xDB\xC9\xCD\xCD\xBC");
	printxy(centerX - 15, centerY - 5, "\xDB\xDB\xBA  \xDB\xDB\xBA \xDB\xDB\xBA \xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 15, centerY - 4, "\xC8\xCD\xBC  \xC8\xCD\xBC \xC8\xCD\xBC \xC8\xCD\xCD\xCD\xCD\xCD\xBC  \xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC");

	printxy(centerX - 6, centerY - 1, "[1]. Jugar");
	printxy(centerX - 6, centerY + 0, "[2]. Instr.");
	printxy(centerX - 6, centerY + 1, "[3]. Creditos");
	printxy(centerX - 6, centerY + 2, "[0]. Salir");
	printxy(centerX - 11, centerY + 5, "Elija una opcion [0...3]");

	color(DARK_YELLOW);
	printxy(centerX - 63, centerY - 6, "	                   X_x					");
	printxy(centerX - 63, centerY - 5, "                      / \\\\\\				");
	printxy(centerX - 63, centerY - 4, "                      |n| |					");
	printxy(centerX - 63, centerY - 3, "                    )(|_|-'X				");
	printxy(centerX - 63, centerY - 2, "                   /  \\Y// \\				");
	printxy(centerX - 63, centerY - 1, "                   |A | | |A|				");
	printxy(centerX - 63, centerY + 1, "                   |  | | |_|				");
	printxy(centerX - 63, centerY + 0, "            )(__X,,|__| | ;;;-,)(,			");
	printxy(centerX - 63, centerY + 1, "           /  \\\\;;;;;;;;;;;;;/    \\		");
	printxy(centerX - 63, centerY + 2, "           |A | |            | U  |			");
	printxy(centerX - 63, centerY + 3, "         )_|  | |____)-----( |    |			");
	printxy(centerX - 63, centerY + 4, "        ///|__|-'////       \\|___)=(__X	");
	printxy(centerX - 63, centerY + 5, "       /////////////         \\///   \\/ \\	");
	printxy(centerX - 63, centerY + 6, "       |           |  U    U |//     \\u|	");
	printxy(centerX - 63, centerY + 7, "       |   )_,-,___|_)=(     | |  U  |_|_X	");
	printxy(centerX - 63, centerY + 8, "       |  ///   \\|//    \\    | |  __ |/// \\");
	printxy(centerX - 63, centerY + 9, "     )_')(//     \\Y/     >---)=( /  \\|  | |-----..,");
	printxy(centerX - 63, centerY + 10, "    //// ,\\ u   u |   u /////   \\|  ||__|A|----.., \\,");
	printxy(centerX - 63, centerY + 11, "   |  | .. |      |    ///// ,-, \\__||--------.., \\, \\,");
	printxy(centerX - 63, centerY + 12, "---'--'_::_|______'----| u | | | |-----------.., \\, \\, \\,");
	printxy(centerX - 63, centerY + 13, "                       |___|_|_|_|----------.., \\, \\, \\, \\,");

	color(BRIGHT_YELLOW);
	printxy(centerX + 23, centerY - 3, "                               %.    ");
	printxy(centerX + 23, centerY - 2, "                               %%=   ");
	printxy(centerX + 23, centerY - 1, "                 %%%%%%        %%#+  ");
	printxy(centerX + 23, centerY + 0, "                %%%%%%%%       %#+:  ");
	printxy(centerX + 23, centerY + 1, "                #%%%%%%%      .#%%#  ");
	printxy(centerX + 23, centerY + 2, "  *###%%###*     %%%%#%      +%%#-#%+");
	printxy(centerX + 23, centerY + 3, " %*::::    +%................%*##%   ");
	printxy(centerX + 23, centerY + 4, " ##::::    *%%%%%%%%%%%%%%%%#$%%+^   ");
	printxy(centerX + 23, centerY + 5, " *%=:::   :%#  ::-#%%%%*:            ");
	printxy(centerX + 23, centerY + 6, "  #%-::   #%     :#%%%%*.            ");
	printxy(centerX + 23, centerY + 7, "   %%+: .%%      .*%%%%#.            ");
	printxy(centerX + 23, centerY + 8, "    +%%%%        .*%%%%#.           ");
	printxy(centerX + 23, centerY + 9, "              .+%#+=+%*%%+.		 ");
	printxy(centerX + 23, centerY + 10, "             =%%=       =%%#        ");
	printxy(centerX + 23, centerY + 11, "            .#%#         *%%#       ");
	printxy(centerX + 23, centerY + 12, "            .%%+          %#%       ");
	
}

void rules() {
	clear();
	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);

	drawBorder(ci);
	color(BRIGHT_CYAN);

	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;

	printxy(centerX - 50, ci.top + 4, "\xDB\xDB\xBB\xDB\xDB\xDB\xBB   \xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xBB   \xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xBB   \xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 50, ci.top + 5, "\xDB\xDB\xBA\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xDB\xDB\xC9\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC");
	printxy(centerX - 50, ci.top + 6, "\xDB\xDB\xBA\xDB\xDB\xC9\xDB\xDB\xBB \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB   \xDB\xDB\xBA   \xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xBA     \xDB\xDB\xBA     \xDB\xDB\xBA\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xC9\xDB\xDB\xBB \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 50, ci.top + 7, "\xDB\xDB\xBA\xDB\xDB\xBA\xC8\xDB\xDB\xBB\xDB\xDB\xBA\xC8\xCD\xCD\xCD\xCD\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xBA     \xDB\xDB\xBA     \xDB\xDB\xBA\xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xBA\xC8\xDB\xDB\xBB\xDB\xDB\xBA\xDB\xDB\xC9\xCD\xCD\xBC  \xC8\xCD\xCD\xCD\xCD\xDB\xDB\xBA");
	printxy(centerX - 50, ci.top + 8, "\xDB\xDB\xBA\xDB\xDB\xBA \xC8\xDB\xDB\xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xBA  \xDB\xDB\xBA\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xBA\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xBA \xC8\xDB\xDB\xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA");
	printxy(centerX - 50, ci.top + 9, "\xC8\xCD\xBC\xC8\xCD\xBC  \xC8\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC   \xC8\xCD\xBC   \xC8\xCD\xBC  \xC8\xCD\xBC \xC8\xCD\xCD\xCD\xCD\xCD\xBC  \xC8\xCD\xCD\xCD\xCD\xCD\xBC \xC8\xCD\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xBC \xC8\xCD\xCD\xCD\xCD\xCD\xBC \xC8\xCD\xBC  \xC8\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC");
	color(BRIGHT_CYAN);
	printxy(centerX - 60, centerY - 3, "CONTROLES:");
	color(WHITE);
	printxy(centerX - 47, centerY - 2, "\xE5 /");
	printxy(centerX - 60, centerY - 1, "   [W]     0(| -");
	printxy(centerX - 60, centerY - 0, "[A][S][D] 	 / \\ ");
	color(BRIGHT_CYAN);
	printxy(centerX - 35, centerY - 3, "CAPSULAS:");
	color(WHITE);
	printxy(centerX - 35, centerY - 2, " ___   ___  ");
	printxy(centerX - 35, centerY - 1, "{ + } { - } ");
	printxy(centerX - 35, centerY - 0, "'---' '---' ");
	color(BRIGHT_CYAN);
	printxy(centerX - 45, centerY + 2, "BARRILES:");
	color(WHITE);
	printxy(centerX - 45, centerY + 3, " _____");
	printxy(centerX - 45, centerY + 4, "/     \\");
	printxy(centerX - 45, centerY + 5, "|     |");
	printxy(centerX - 45, centerY + 6, "|     |");
	printxy(centerX - 45, centerY + 7, "|     |");
	printxy(centerX - 45, centerY + 8, "\\_____/");

	color(DARK_YELLOW);
	printxy(centerX - 19, centerY - 3, "Leiko, es un chico que se aventuro a una peligrosa y rara aventura dentro de");
	printxy(centerX - 19, centerY - 2, "un castillo de miel en el bosque, donde su hermana fue secuestrada.");
	printxy(centerX - 19, centerY - 1, "El sabe lo peligroso que es entrar al castillo, asi que se armo con las");
	printxy(centerX - 19, centerY - 0, "armas mas peligrosas del mundo, un palo de madera a la que llamo chuchumaru ");
	printxy(centerX - 19, centerY + 1, "y una tapa de un bote de basura.");

	color(BRIGHT_CYAN);
	printxy(centerX - 19, centerY + 3, "Recoge las capsulas de energia pasando por encima de ellas y luego llevalas.");
	printxy(centerX - 19, centerY + 4, "a los barriles, depositalas con [E] y quitalas con [R].");
	printxy(centerX - 19, centerY + 4, "Tu objetivo sera llegar a la cantidad de energia pedida por el ultimo");
	printxy(centerX - 19, centerY + 5, "contenedor, ten cuidado porque habra capsulas con energia negativa en el");
	printxy(centerX - 19, centerY + 6, "camino, las cuales tambien podrias depositarlas en los barriles ");

	printxy(centerX + 8, centerY + 8, "VAMOS LEIKO TU PUEDES!");
	printxy(centerX - 40, centerY + 12, "Presiona la tecla [9] para volver al menu.");
	esperarTecla('9');
}

void credits() {
	clear();

	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);
	drawBorder(ci);

	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;

	printxy(centerX - 30, centerY - 12, " \xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xBB \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB ");
	printxy(centerX - 30, centerY - 11, "\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xBA\xC8\xCD\xCD\xDB\xDB\xC9\xCD\xCD\xBC\xDB\xDB\xC9\xCD\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xC9\xCD\xCD\xCD\xCD\xBC");
	printxy(centerX - 30, centerY - 10, "\xDB\xDB\xBA     \xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xDB\xDB\xDB\xBB  \xDB\xDB\xBA  \xDB\xDB\xBA\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB");
	printxy(centerX - 30, centerY - 9, "\xDB\xDB\xBA     \xDB\xDB\xC9\xCD\xCD\xDB\xDB\xBB\xDB\xDB\xC9\xCD\xCD\xBC  \xDB\xDB\xBA  \xDB\xDB\xBA\xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xBA   \xDB\xDB\xBA\xC8\xCD\xCD\xCD\xCD\xDB\xDB\xBA");
	printxy(centerX - 30, centerY - 8, "\xC8\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xBA  \xDB\xDB\xBA\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBB\xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xBA   \xDB\xDB\xBA   \xC8\xDB\xDB\xDB\xDB\xDB\xDB\xC9\xBC\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xBA");
	printxy(centerX - 30, centerY - 7, " \xC8\xCD\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xBC  \xC8\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC\xC8\xCD\xCD\xCD\xCD\xCD\xBC \xC8\xCD\xBC   \xC8\xCD\xBC    \xC8\xCD\xCD\xCD\xCD\xCD\xBC \xC8\xCD\xCD\xCD\xCD\xCD\xCD\xBC ");

	printxy(centerX - 10, centerY - 4, "- Antonio Ropon");
	printxy(centerX - 10, centerY - 3, "- Fabrizio Cueva");
	printxy(centerX - 10, centerY - 2, "- Carlos Cespedes");

	color(DARK_RED);
	printxy(centerX - 13, centerY + 1, "         **           ");
	printxy(centerX - 13, centerY + 2, "    *   ***     *     ");
	printxy(centerX - 13, centerY + 3, "  **   ****      **   ");
	printxy(centerX - 13, centerY + 4, " ***   ******     **  ");
	printxy(centerX - 13, centerY + 5, " ***    ******    **  ");
	printxy(centerX - 13, centerY + 6, " ***     *****   **** ");
	printxy(centerX - 13, centerY + 7, " ****     ****  ****  ");
	printxy(centerX - 13, centerY + 8, " *******  *** ******  ");
	printxy(centerX - 13, centerY + 9, "   ****************   ");
	printxy(centerX - 13, centerY + 10, "    *************     ");
	printxy(centerX - 13, centerY + 11, "       *******        ");

	printxy(centerX + 20, centerY + 12, "Presiona la tecla [9] para volver al menu.");

	esperarTecla('9');
}

void error() {
	clear();
	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);
	drawBorder(ci);
	color(BRIGHT_RED);
	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;

	printxy(centerX - 15, centerY - 9, " _________________ ___________ ");
	printxy(centerX - 15, centerY - 8, "|  ___| ___ | ___ |  _  | ___ \\");
	printxy(centerX - 15, centerY - 7, "| |__ | |_/ | |_/ | | | | |_/ /");
	printxy(centerX - 15, centerY - 6, "|  __||    /|    /| | | |    / ");
	printxy(centerX - 15, centerY - 5, "| |___| |\\ \\| |\\ \\ \\_/ | |\\ \\ ");
	printxy(centerX - 15, centerY - 4, "\\____/\\_| \\_\\_| \\_|\\___/\\_| \\_|");
	printxy(centerX - 30, centerY - 0, "Has presionado un digito erroneo. Vuelve al menu con el boton: [9]");
	color(WHITE);
	esperarTecla('9');

}

void gameOver() {
	clear();

	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);
	drawBorder(ci);

	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;

	color(BRIGHT_RED);

	printxy(centerX - 10, centerY - 10, "     .-******-.     ");
	printxy(centerX - 10, centerY - 9, "   .'          '.	 ");
	printxy(centerX - 10, centerY - 8, "  /   O      O   \\ ");
	printxy(centerX - 10, centerY - 7, " :           `    : ");
	printxy(centerX - 10, centerY - 6, " |                | ");
	printxy(centerX - 10, centerY - 5, " :    .------.    : ");
	printxy(centerX - 10, centerY - 4, "  \\  '        '  / ");
	printxy(centerX - 10, centerY - 3, "   '.          .'	 ");
	printxy(centerX - 10, centerY - 2, "     '-......-'	 ");

	printxy(centerX - 15, centerY + 1, "Lo siento, perdiste");

	printxy(centerX - 15, centerY + 3, "Fuiste una buena persona... "); ;
	printxy(centerX - 15, centerY + 5, "Presiona CUALQUIER TECLA para salir."); ;

	color(WHITE);
	_getch();
	clear();
	exit(EXIT_SUCCESS);
}

void nextLevel() {
	clear();
	ConsoleInfo ci;
	getConsoleInfo(&ci, 0, 0, 0, 0);
	drawBorder(ci);
	int centerX = (ci.right - ci.left + 1) / 2;
	int centerY = (ci.bottom - ci.top + 1) / 2;
	color(DARK_YELLOW);

	printxy(centerX - 10, centerY - 9, "   ___________    	");
	printxy(centerX - 10, centerY - 8, "  '._==_==_=_.'	");
	printxy(centerX - 10, centerY - 7, "  .-\\:      /-.	");
	printxy(centerX - 10, centerY - 6, " | (|:. NEXT |) |	");
	printxy(centerX - 10, centerY - 5, "  '-|:. LEVEL|-'	");
	printxy(centerX - 10, centerY - 4, "    \\::.    /		");
	printxy(centerX - 10, centerY - 3, "     '::. .'		");
	printxy(centerX - 10, centerY - 2, "       ) (		");
	printxy(centerX - 10, centerY - 1, "     _.' '._		");
	printxy(centerX - 10, centerY - 0, "    ---------		");

	printxy(centerX - 20, centerY + 2, "Felicidades, ganaste! Eso estuvo cerca jeje");
	printxy(centerX - 20, centerY + 3, "Pero la aventura continua...");

	printxy(centerX - 16, centerY + 5, "Presiona cualquier tecla....");

	color(WHITE);
	_getch();

}


void updateCapsuleCounter(int count) {
	gotoxy(5, 2);
	color(DARK_YELLOW);
	cout << "RECOLECTA: " << "[" << count << "]";
	gotoxy(59, 25);
	cout << "+";
	gotoxy(74, 25);
	cout << "+";
	gotoxy(89, 25);
	cout << "+";
	color(BRIGHT_CYAN);
	gotoxy(104, 25);
	cout << "=";
	color(WHITE);
}

void updateCapsuleCounter2(int count) {
	gotoxy(5, 3);
	color(DARK_YELLOW);
	cout << "RECOLECTA: " << "[" << count << "]";
	gotoxy(74, 25);
	cout << "+";
	gotoxy(89, 25);
	cout << "+";
	color(BRIGHT_CYAN);
	gotoxy(104, 25);
	cout << "=";
	color(WHITE);
}