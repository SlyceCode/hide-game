﻿#pragma once
#include <iostream>
#include <cstdlib>  
#include <ctime>    
#include "upc.h"
#include "Character.h"
#include "Displays.h"
#include "allies.h" 

using namespace std;

#define MONSTER_HEIGHT1 10
#define MONSTER_WIDTH1 12
#define MONSTER_HEIGHT2 8
#define MONSTER_WIDTH2 27
#define MONSTER_HEIGHT3 13
#define MONSTER_WIDTH3 25

int centerX = 50;
int centerY = 12;

struct Monster {
    int x, y;
    int health;
    string sprite1[MONSTER_HEIGHT1] = {
        " |\\______//|",
        " |/      \\ |  /|",
        " / _   _  `|_ ||",
        " | O)  O   ) \\|| ",
        " \\        /  | | ",
        "/ \\ Y    /'  | \\  ",
        "|  `._/' |   |  \\",
        "|  |`-._/    / _ \\",
        "\\   \\   ./.../ \\  \\ ",
        " \\uU\\\\UU/    |____/ "
    };
    string sprite2[MONSTER_HEIGHT2] = {
        " .-'`\\-,/^\\ .-.",
        " /    |  \\  ( ee\\   __",
        " |     |  |__/,--.`\\'`  `,",
        " |    /   .__/    `\\'\\'\\',/",
        " |   /    /  |",
        " .'.-'    /__/",
        " `\\'`| |';-;_`",
        " |/ /-))))))"
    };
    string sprite3[MONSTER_HEIGHT3] = {
        "      \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB",
        "   \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB",
        " \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB",
        "\xDB\xDB\xDB\xDB\xDB\xDB      \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB      \xDB\xDB\xDB\xDB\xDB",
        "\xDB\xDB\xDB    \xDB\xDB\xDB   \xDB\xDB\xDB\xDB\xDB\xDB   \xDB\xDB\xDB    \xDB\xDB",
        "\xDB\xDB\xDB   \xDB\xDB \xDB\xDB  \xDB\xDB\xDB\xDB\xDB\xDB  \xDB\xDB \xDB\xDB   \xDB\xDB",
        " \xDB\xDB\xDB        \xDB\xDB\xDB  \xDB\xDB\xDB        \xDB\xDB",
        "  \xDB\xDB\xDB\xDB\xDB\xDB   \xDB\xDB\xDB    \xDB\xDB\xDB   \xDB\xDB\xDB\xDB\xDB",
        " \xDB\xDB\xDB\xDB\xDB  \xDB\xDB\xDB\xDB\xDB\xDB    \xDB\xDB\xDB\xDB\xDB\xDB\xDB  \xDB\xDB\xDB",
        " \xDB\xDB\xDB                        \xDB\xDB",
        "  \xDB\xDB\xDB\xDB\xDB   \xDB\xDB \xDB\xDB \xDB\xDB \xDB \xDB\xDB   \xDB\xDB\xDB\xDB",
        "    \xDB\xDB\xDB\xDB\xDB\xDB             \xDB\xDB\xDB\xDB",
        "          \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB",
    };
};

Monster createMonster(int startX, int startY) {
    Monster monster;
    monster.x = startX;
    monster.y = startY;
    monster.health = 100;
    return monster;
}

void drawOptionBox(int x, int y, int width, int height, const string& text) {
    gotoxy(x, y);
    cout << "\xC9";
    for (int i = 0; i < width - 2; ++i) cout << "\xCD";
    cout << "\xBB";

    gotoxy(x, y + 1);
    cout << "\xBA " << text;
    gotoxy(x + width - 1, y + 1);
    cout << "\xBA";

    gotoxy(x, y + height - 1);
    cout << "\xC8";
    for (int i = 0; i < width - 2; ++i) cout << "\xCD";
    cout << "\xBC";
}

void clearMenuBox(int x, int y, int width, int height) {
    for (int i = 0; i < height; ++i) {
        gotoxy(x, y + i);
        cout << string(width, ' ');
    }
}


void drawMonster(const Monster& monster, int spriteNumber = 1) {
    const string* sprite;
    int spriteHeight;
    switch (spriteNumber) {
    case 2:
        sprite = monster.sprite2;
        spriteHeight = MONSTER_HEIGHT2;
        break;
    case 3:
        sprite = monster.sprite3;
        spriteHeight = MONSTER_HEIGHT3;
        break;
    default:
        sprite = monster.sprite1;
        spriteHeight = MONSTER_HEIGHT1;
        break;
    }

    color(BRIGHT_RED);
    for (int i = 0; i < spriteHeight; ++i) {
        gotoxy(monster.x, monster.y + i);
        cout << sprite[i];
    }
    color(WHITE);
}


void clearMonster(const Monster& monster, int spriteNumber = 1) {
    const string* sprite;
    int spriteHeight;
    switch (spriteNumber) {
    case 2:
        sprite = monster.sprite2;
        spriteHeight = MONSTER_HEIGHT2;
        break;
    case 3:
        sprite = monster.sprite3;
        spriteHeight = MONSTER_HEIGHT3;
        break;
    default:
        sprite = monster.sprite1;
        spriteHeight = MONSTER_HEIGHT1;
        break;
    }

    for (int i = 0; i < spriteHeight; ++i) {
        gotoxy(monster.x, monster.y + i);
        cout << string(sprite[i].length(), ' ');
    }
}

bool checkMonsterCollision(const Character& character, const Monster& monster, int spriteNumber = 1) {
    int charLeft = character.x;
    int charRight = character.x + SPRITE_WIDTH;
    int charTop = character.y;
    int charBottom = character.y + SPRITE_HEIGHT;

    int monsterLeft = monster.x;
    int monsterRight, monsterTop, monsterBottom;
    switch (spriteNumber) {
    case 2:
        monsterRight = monster.x + MONSTER_WIDTH2;
        monsterBottom = monster.y + MONSTER_HEIGHT2;
        break;
    case 3:
        monsterRight = monster.x + MONSTER_WIDTH3;
        monsterBottom = monster.y + MONSTER_HEIGHT3;
        break;
    default:
        monsterRight = monster.x + MONSTER_WIDTH1;
        monsterBottom = monster.y + MONSTER_HEIGHT1;
        break;
    }
    monsterTop = monster.y;

    return charRight > monsterLeft &&
        charLeft < monsterRight &&
        charBottom > monsterTop &&
        charTop < monsterBottom;
}

void drawAttackBar(int centerX, int centerY) {
    int barLength = 30;
    int barPos = centerX - barLength / 2;

    gotoxy(barPos, centerY);
    for (int i = 0; i < barLength; ++i) {
        cout << "\xB1";
    }

    gotoxy(centerX, centerY);
    cout << "|";
}

void animateAttackBar(int centerX, int centerY, int& hitPosition) {
    int barLength = 30;
    int barPos = centerX - barLength / 2;
    bool direction = true;

    int position = 0;

    while (true) {
        if (direction && position > 0) {
            gotoxy(barPos + position - 1, centerY);
            cout << "\xB1";
        }
        else if (!direction && position < barLength - 1) {
            gotoxy(barPos + position + 1, centerY);
            cout << "\xB1";
        }

        gotoxy(barPos + position, centerY);
        cout << "\xDB";

        if (_kbhit()) {
            if (_getch() == ' ') {
                hitPosition = position;
                return;
            }
        }

        Sleep(50);

        if (direction) {
            position++;
            if (position == barLength) {
                direction = false;
                position--;
            }
        }
        else {
            position--;
            if (position < 0) {
                direction = true;
                position++;
            }
        }

        gotoxy(centerX, centerY);
        cout << "|";
    }
}

void spriteCharacter() {
    gotoxy(20, 5);
    cout << "   -@@@@@@@@.           ";
    gotoxy(20, 6);
    cout << "  @%       *@%        +#";
    gotoxy(20, 7);
    cout << "-@          :@%      *@+";
    gotoxy(20, 8);
    cout << "@@           *@     *@+ ";
    gotoxy(20, 9);
    cout << "@@           %@    *@3- ";
    gotoxy(20, 10);
    cout << " @@         #@   @%#@#@ ";
    gotoxy(20, 11);
    cout << "  *@@@  @#@@%     @+    ";
    gotoxy(20, 12);
    cout << "      @@                ";
    gotoxy(20, 13);
    cout << " .@@= #@     #@#        ";
    gotoxy(20, 14);
    cout << ".@@.  #@@*.#@%.         ";
    gotoxy(20, 15);
    cout << ".@@.  #@.@@@.           ";
    gotoxy(20, 16);
    cout << " +@#@:#@                ";
    gotoxy(20, 17);
    cout << "     +@@%.              ";
    gotoxy(20, 18);
    cout << "   +@@  #@#.            ";
    gotoxy(20, 19);
    cout << "   @@    % @.           ";
}

void spriteAliado() {
    gotoxy(35, 14);
    cout << "             ____  ";
    gotoxy(35, 15);
    cout << "            / . .\\ ";
    gotoxy(35, 16);
    cout << "            \\  ---<";
    gotoxy(35, 17);
    cout << "             \\  /  ";
    gotoxy(35, 18);
    cout << "   __________/ /   ";
    gotoxy(35, 19);
    cout << "-=:___________/    ";
}
void spriteMonster1() {
    color(BRIGHT_RED);
    gotoxy(95, 8);
    cout << " |\\______//|";
    gotoxy(95, 9);
    cout << " |/      \\ |  /|";
    gotoxy(95, 10);
    cout << " / _   _  `|_ ||";
    gotoxy(95, 11);
    cout << " | O)  O   ) \\|| ";
    gotoxy(95, 12);
    cout << " \\        /  | | ";
    gotoxy(95, 13);
    cout << "/ \\ Y    /'  | \\  ";
    gotoxy(95, 14);
    cout << "|  `._/' |   |  \\";
    gotoxy(95, 15);
    cout << "|  |`-._/    / _ \\";
    gotoxy(95, 16);
    cout << "\\   \\   ./.../ \\  \\ ";
    gotoxy(95, 17);
    cout << " \\uU\\\\UU/    |____/ ";
    color(WHITE);
}


void spriteMonster2() {
    color(BRIGHT_RED);
    gotoxy(85, 8);
    cout << "           .-. /^\\,-/`'-.";
    gotoxy(85, 9);
    cout << "  __   /aa )  /   |     \\  ";
    gotoxy(85, 10);
    cout << ",'  `'`.--,\\__|   |     | ";
    gotoxy(85, 11);
    cout << "\\,´´´´`    \\__.  \\      | ";
    gotoxy(85, 12);
    cout << "           |  \\   \\     | ";
    gotoxy(85, 13);
    cout << "            \\__\\   '-. '.";
    gotoxy(85, 14);
    cout << "              `_;-;'| |`'";
    gotoxy(85, 15);
    cout << "            ((((((-\\ \\|  ";
}

void spriteMonster3() {
    color(BRIGHT_RED);
    gotoxy(88, 6);
    cout << "      \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB";
    gotoxy(88, 7);
    cout << "   \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB";
    gotoxy(88, 8);
    cout << " \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB";
    gotoxy(88, 9);
    cout << "\xDB\xDB\xDB\xDB\xDB\xDB      \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB      \xDB\xDB\xDB\xDB\xDB";
    gotoxy(88, 10);
    cout << "\xDB\xDB\xDB    \xDB\xDB\xDB   \xDB\xDB\xDB\xDB\xDB\xDB   \xDB\xDB\xDB    \xDB\xDB";;
    gotoxy(88, 11);
    cout << "\xDB\xDB\xDB   \xDB\xDB \xDB\xDB  \xDB\xDB\xDB\xDB\xDB\xDB  \xDB\xDB \xDB\xDB   \xDB\xDB";
    gotoxy(88, 12);
    cout << " \xDB\xDB\xDB        \xDB\xDB\xDB  \xDB\xDB\xDB        \xDB\xDB";
    gotoxy(88, 13);
    cout << "  \xDB\xDB\xDB\xDB\xDB\xDB   \xDB\xDB\xDB    \xDB\xDB\xDB   \xDB\xDB\xDB\xDB\xDB";
    gotoxy(88, 14);
    cout << " \xDB\xDB\xDB\xDB\xDB  \xDB\xDB\xDB\xDB\xDB\xDB    \xDB\xDB\xDB\xDB\xDB\xDB\xDB  \xDB\xDB\xDB";
    gotoxy(88, 15);
    cout << " \xDB\xDB\xDB                        \xDB\xDB";
    gotoxy(88, 16);
    cout << "  \xDB\xDB\xDB\xDB\xDB   \xDB\xDB \xDB\xDB \xDB\xDB \xDB \xDB\xDB   \xDB\xDB\xDB\xDB";
    gotoxy(88, 17);
    cout << "    \xDB\xDB\xDB\xDB\xDB\xDB             \xDB\xDB\xDB\xDB";
    gotoxy(88, 18);
    cout << "          \xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB\xDB";
}


void displayAttackResult(int damage) {
    color(BRIGHT_GREEN);
    gotoxy(centerX - 33, 3);
    cout << "TU ATAQUE FUE EFECTIVO";
    gotoxy(centerX + 46, 19);
    cout << "-" << damage;
}

void displayMonsterAttack(int monsterDamage) {
    color(BRIGHT_RED);
    gotoxy(centerX + 42, 5);
    cout << "LANZO UN ATAQUE";
    gotoxy(centerX - 30, 21);
    cout << "-" << monsterDamage;
}

int calculateDamage(int hitPosition) {
    if (hitPosition > 13 && hitPosition < 17) {
        gotoxy(centerX + 12, centerY + 4);
        color(BRIGHT_GREEN);
        cout << "Golpe critico!";
        return 30;
    }
    else if (hitPosition > 10 && hitPosition < 20) {
        gotoxy(centerX + 14, centerY + 4);
        color(BRIGHT_YELLOW);
        cout << "Buen golpe!";
        return 20;
    }
    else {
        gotoxy(centerX + 14, centerY + 4);
        color(BRIGHT_BLACK);
        cout << "Golpe debil...";
        return 10;
    }
}

void handleSpecialAttack(Character& character, Monster& monster, int centerX, int centerY, int& waterUses) {
    if (waterUses <= 0) {
        gotoxy(centerX - 10, 14);
        cout << "Ya no tienes mas usos de Lanzarle Agua.";
        return;
    }

    srand(time(0));
    int a = (rand() % 11) - 5;
    while (a == 0) {
        a = (rand() % 11) - 5;
    }
    int b = rand() % 11 - 5;
    int x = rand() % 21 - 10;
    int result = a * x + b;

    gotoxy(centerX - 10, 14);
    cout << "Resuelve la ecuacion para un ataque especial: ";
    gotoxy(centerX - 10, 15);
    color(BRIGHT_CYAN);
    cout << a << "x + " << b << " = " << result;
    gotoxy(centerX - 10, 16);
    color(BRIGHT_YELLOW);
    cout << "Presiona 'h' para una pista o 'Enter' para continuar: ";

    bool hintUsed = false;
    while (true) {
        char useHint = _getch();
        if (useHint == 'h' || useHint == 'H') {
            hintUsed = true;
            gotoxy(centerX - 10, 17);
            int hintRange = rand() % 3 + 1;
            cout << "Pista: La solucion esta entre " << (x - hintRange) << " y " << (x + hintRange) << ".";
            gotoxy(centerX - 10, 18);
            color(BRIGHT_GREEN);
            cout << "Intenta de nuevo: x = ";
            break;
        }
        else if (useHint == 13) {
            gotoxy(centerX - 10, 18);
            cout << "x = ";
            break;
        }
    }

    int playerAnswer;
    cin >> playerAnswer;

    if (playerAnswer == x) {
        int damage = 40;
        monster.health -= damage;
        gotoxy(centerX - 10, 19);
        color(BRIGHT_GREEN);
        cout << "Respuesta correcta! Infliges " << damage << " de damage.";
    }
    else {
        gotoxy(centerX - 10, 19);
        color(BRIGHT_RED);
        if (hintUsed) {
            cout << "Respuesta incorrecta, :(.";
        }
        else {
            cout << "Respuesta incorrecta. Pudiste usar tu pista.";
        }
    }

    gotoxy(centerX + 49, 19);
    color(BRIGHT_RED);
    printf("+%d/100 HP", monster.health);

    waterUses--;
}

void handleFindMissingNumber(Character& character, Monster& monster, int centerX, int centerY, int& shieldUses) {
    if (shieldUses <= 0) {
        gotoxy(centerX - 10, 14);
        cout << "Ya no tienes mas usos de Carga de Escudo.";
        return;
    }

    srand(time(0));

    int a = (rand() % 11);
    int b = (rand() % 21) - 10;
    int missingNumber = b - a;

    gotoxy(centerX - 10, 14);
    cout << "Encuentra el numero perdido en la ecuacion: ";
    gotoxy(centerX - 10, 15);
    cout << a << " + _ = " << b;
    gotoxy(centerX - 10, 16);
    cout << "_ = ";

    int playerAnswer;
    cin >> playerAnswer;

    if (playerAnswer == missingNumber) {
        int damage = 35;
        monster.health -= damage;
        gotoxy(centerX - 10, 17);
        color(BRIGHT_GREEN);
        cout << "Respuesta correcta! Infliges " << damage << " de damage.";
    }
    else {
        gotoxy(centerX - 10, 17);
        color(BRIGHT_RED);
        cout << "Respuesta incorrecta. No se realiza el ataque especial.";
    }

    gotoxy(centerX + 49, 19);
    color(BRIGHT_RED);
    printf("+%d/100 HP", monster.health);

    shieldUses--;
}

void handleAttack(Character& character, Monster& monster, int centerX, int centerY, int& shieldUses, int& waterUses) {
    int monsterAttack = 15;
    int menuY = 24;

    clearMenuBox(20, 24, 90, 3);

    drawOptionBox(32, menuY, 16, 3, "[1] Espadazo");

    char waterOption[30];
    char shieldOption[30];

    sprintf_s(waterOption, " [2] Echar Agua %d/1", waterUses);
    sprintf_s(shieldOption, "[3] Carga de Escudo %d/2", shieldUses); // el sprint_s lo sugiere VS

    drawOptionBox(49, menuY, 25, 3, waterOption);
    drawOptionBox(75, menuY, 28, 3, shieldOption);

    char attackAction;
    attackAction = _getch();

    int damage = 0;
    switch (attackAction) {
    case '1':
        gotoxy(centerX - 2, 13);
        cout << "Presiona la barra espaciadora para atacar:";
        int hitPosition;
        drawAttackBar(centerX + 19, centerY + 3);
        animateAttackBar(centerX + 19, centerY + 3, hitPosition);
        damage = calculateDamage(hitPosition);
        monster.health -= damage;
        displayAttackResult(damage);
        break;
    case '2':
        if (waterUses > 0) {
            handleSpecialAttack(character, monster, centerX, centerY, waterUses);
        }
        else {
            gotoxy(centerX - 10, 14);
            cout << "Ya no tienes mas usos de Lanzarle Agua.";
        }
        break;
    case '3':
            if (shieldUses > 0) {
            handleFindMissingNumber(character, monster, centerX, centerY, shieldUses);
        }
        else {
            gotoxy(centerX - 10, 14);
            cout << "Ya no tienes mas usos de Carga de Escudo.";
        }
        break;
    default:
        gotoxy(centerX - 10, 14);
        cout << "Opción invalida.";
        break;
    }

    gotoxy(centerX + 49, 19);
    color(BRIGHT_RED);
    printf("+%d/100 HP", monster.health);

    if (monster.health <= 0) {
        gotoxy(centerX, 10);
        cout << "Se observa un charco de sangre...";
        return;
    }

    int monsterDamage = rand() % monsterAttack + 5;
    character.health -= monsterDamage;
    displayMonsterAttack(monsterDamage);

    gotoxy(centerX - 27, 21);
    color(BRIGHT_GREEN);
    printf("+%d/100 HP", character.health);

    if (character.health <= 0) {
        gotoxy(centerX - 20, 10);
        clear();
    }
}

void handleItem(Character& character, int centerX, int centerY, int menuY) {
    int optionWidth = 16;
    int optionHeight = 3;

    color(BRIGHT_GREEN);
    clearMenuBox(20, menuY, 90, optionHeight);
    drawOptionBox(53, menuY, optionWidth, optionHeight, "1. Pocion");
    drawOptionBox(71, menuY, optionWidth, optionHeight, "2. Juguito");

    char itemAction;
    itemAction = _getch();

    if (itemAction == '1') {
        character.health += 10;
        if (character.health > 100) character.health = 100;
        gotoxy(centerX - 5, 14);
        color(BRIGHT_GREEN);
        cout << "Usaste una pocion. Salud (+10) recuperada a " << character.health << " HP.";
    }
    else if (itemAction == '2') {
        character.health += 20;
        if (character.health > 100) character.health = 100;
        gotoxy(centerX - 5, 14);
        color(BRIGHT_GREEN);
        cout << "Usaste un juguito. Salud (+20) recuperada a " << character.health << " HP.";
    }

    gotoxy(centerX - 27, 21);
    color(BRIGHT_GREEN);
    cout << "+" << character.health << "/100 HP";
}

void handleDialogue(int centerX, int centerY, int menuY) {
    int optionWidth = 16;
    int optionHeight = 3;

    clearMenuBox(23, menuY, 90, optionHeight);
    drawOptionBox(43, menuY, optionWidth, optionHeight, "1. Acariciar");
    drawOptionBox(61, menuY, optionWidth, optionHeight, "2. Piedra");
    drawOptionBox(79, menuY, optionWidth, optionHeight, "3. Sentarse");

    char dialogAction;
    dialogAction = _getch();

    if (dialogAction == '1') {
        gotoxy(centerX - 5, 14);
        color(BRIGHT_YELLOW);
        cout << "Intento morderte, pero lo esquivaste a tiempo!";
    }
    else if (dialogAction == '2') {
        gotoxy(centerX + 7, 14);
        color(BRIGHT_YELLOW);
        cout << "Ya se conoce ese truco";
    }
    else if (dialogAction == '3') {
        gotoxy(centerX + 7, 14);
        color(BRIGHT_YELLOW);
        cout << "Ya esta sentado...";
    }
}

void handleSummonAlly(Monster& monster, int centerX, int centerY, int& allyUses) {
    if (allyUses <= 0) {
        gotoxy(centerX + 2, centerY);
        cout << "Ya no puedes invocar mas aliados.";
        return; 
    }

    Ally ally = createAlly(centerX - 15, centerY + 3); 
    drawAlly(ally);

    int allyDamage = rand() % 20 + 10;
    monster.health -= allyDamage;

    gotoxy(centerX + 2, centerY);
    color(BRIGHT_GREEN);
    cout << "Tu aliado inflige " << allyDamage << " puntos de damage.";

    gotoxy(centerX + 49, 19);
    color(BRIGHT_RED);
    printf("+%d/100 HP", monster.health);

    allyUses--;
}

void combatSystem(Character& character, Monster& monster, bool useSecondSprite, bool useThirdSprite = false) {
    srand(static_cast<unsigned int>(time(nullptr)));

    int monsterAttack = 15;
    int centerX = 50;
    int centerY = 12;

    int shieldUses = 2;
    int waterUses = 1;
    int allyUses = 1;

    clear();
    gotoxy(centerX - 20, 1);

    while (character.health > 0 && monster.health > 0) {
        clear();
        gotoxy(centerX, 1);

        if (useThirdSprite) {
            cout << "ENTRASTE EN COMBATE CON SANS";
        }
        else if (useSecondSprite) {
            cout << "ENTRASTE EN COMBATE CON ESPECTRO";
        }
        else {
            cout << "ENTRASTE EN COMBATE CON DESTROZAMUNDOS";
        }

        gotoxy(centerX - 27, 21);
        color(BRIGHT_GREEN);
        cout << "+" << character.health << "/100 HP";

        gotoxy(centerX - 17, 23);
        color(BRIGHT_RED);
        gotoxy(centerX + 49, 19);
        cout << "+" << monster.health << "/100 HP";

        color(BRIGHT_GREEN);
        spriteCharacter();
        color(WHITE);

        if (useThirdSprite) {
            spriteMonster3();
        }
        else if (useSecondSprite) {
            spriteMonster2();
        }
        else {
            spriteMonster1();
        }

        int menuY = 24;
        int optionWidth = 16;
        int optionHeight = 3;

        color(BRIGHT_YELLOW);
        drawOptionBox(33, menuY, optionWidth, optionHeight, "1. Atacar");
        drawOptionBox(51, menuY, optionWidth, optionHeight, "2. Usar Item");
        drawOptionBox(69, menuY, optionWidth, optionHeight, "3. Actuar");

        char allyOption[30];
        sprintf_s(allyOption, "[4] Aliado %d/1", allyUses);

        drawOptionBox(87, menuY, optionWidth + 2, optionHeight, allyOption);

        char action;
        action = _getch();

        switch (action) {
        case '1':
            handleAttack(character, monster, centerX, centerY, shieldUses, waterUses);
            break;
        case '2':
            handleItem(character, centerX, centerY, menuY);
            break;
        case '3':
            handleDialogue(centerX, centerY, menuY);
            break;
        case '4':
            handleSummonAlly(monster, centerX, centerY, allyUses);
            break;
        }

        gotoxy(centerX - 5, centerY + 10);
        color(BRIGHT_YELLOW);
        if (character.health > 0) {
            cout << "Presiona cualquier tecla para ver los resultados";
            _getch();
        }
        else {
            gameOver();
            return;  
        }
        color(WHITE);
    }
}